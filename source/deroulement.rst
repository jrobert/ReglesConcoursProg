Déroulement du concours
=======================

Objectif
++++++++

L'objectif est de créer un *agent* (intelligence artificielle) contrôlant un avatar qui se déplace à l'intérieur d'un labyrinthe en essayant de maximiser son score.

Programme de la journée
+++++++++++++++++++++++

* *8h30 -  9h30* : Accueil des participants
* *9h30 - 10h* : Présentation du concours et distribution du matériel
* *10h - 12h30* : Développement des *agents* (par équipe)
* *12h30 - 13h30* : Plage de restauration
* *13h30 - 15h30* : Suite du développement des *agents* (par équipe)
* *15h30 - 16h* : Qualifications
* *16h - 17h* : Développement (modifications et corrections) des *agents* (par équipe)
* *17h - 18h* : Tournoi par poules
* *18h - 18h15* : Finale du tournoi et distribution des lots.


Précisions sur le déroulement de la journée
++++++++++++++++++++++++++++++++++++++++++++

1. **Développement des agents**

  Entre 10h et 15h30, vous développerez votre *agent* dans le langage de votre choix (Python ou Java).
  Vous disposez ainsi d'environ 5 heures de développement si vous prenez 30 minutes pour vous restaurer.

2. **Qualifications**

  Les qualifications auront lieu au terme de cette période de développement.
  Pendant cette phase, les agents de chaque équipe seront mis en confrontation.
  Ces qualifications ne sont pas éliminatoires. Elles permettent simplement
  d'effectuer un premier classement afin de répartir les équipes dans les
  différentes poules pour le début du tournoi. Ces qualifications vous
  permetteront également d'étudier le comportement de votre *agent* en "conditions réelles".

3. **Modifications et corrections des agents**

  Suite aux qualifications, vous disposerez d'une heure de développement supplémentaire,
  afin d'apporter des modifications et/ou corrections à vos *agents* avant le
  lancement du tournoi.

4. **Tournoi**

  Au terme de cette longue journée de développement, le tournoi pourra commencer.
 
 
Précisions sur le déroulement du tournoi
+++++++++++++++++++++++++++++++++++++++++

Le tournoi se déroule de la manière suivante :

   * Les équipes s'affrontent par poules de 3 ou 4 équipes. À chaque tour, les deux premières équipes de chaque poule se qualifient pour le tour suivant.
   
   * Le classement de la poule finale déterminera les vainqueurs du concours


Un **prix spécial** sera attribué aux vainqueurs du mode **seul contre tous**. Dans ce mode toutes les équipes seront sur le plateau. L'équipe qui accumulera le plus de points au cours de trois manches sera déclarée vainqueur du mode **seul contre tous** !



Questions
+++++++++

Vous pouvez poser vos question ou faire remonter des bugs sur le discord : `Concours Programmation IUTO <https://discord.gg/4BKUa5s>`_  
