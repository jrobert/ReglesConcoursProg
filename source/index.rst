.. Regles Concours de Programmation documentation master file, created by
   sphinx-quickstart on Tue Feb  6 14:43:30 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Règles Concours 
================

.. toctree::
   :numbered:
   :maxdepth: 2

   jeu/presentation_du_concours
   deroulement
   jeu/2_client_serveur 
   jeu/3_protocole
   annexe

.. toctree::   
   :hidden:
   
   jeu/index
   jeu/protocole

