Annexe
======

Génération de carte
+++++++++++++++++++

Cette section s'adresse aux personnes qui ont envie de créer leur propres cartes. Elle n'est pas utile pour créer une IA.

Les cartes sont au format json. Inutile de savoir créer un tel fichier, il suffit de le générer automatiquement avec un outil adapté comme par exemple Tiled (www.mapeditor.org). 

Les contraintes pour qu'une carte générée par Tiled fonctionne avec le serveur : 

* Le Tileset doit être nommé ``Atlas`` . 

* Les objets qu'il est impossible de traverser doivent être dans un calque nommé ``Ground`` . 

* Pour qu'un objet puisse être détruit, il doit avoir la propriété ``cassable`` à true. 

* La propriété ``probability``, comprise entre 0 et 1 permet de créer des objets aléatoirement.


