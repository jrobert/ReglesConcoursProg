Echanges Client/Serveurs
===========================================

Généralités
++++++++++++

JSON
----

Les échanges avec le serveur se font avec des chaines de caractères ascii contenant des données au format json. 

Pour votre sécurité, ne générez pas les chaines vous mêmes, utilisez une bibliothèque qui "dump" vos données au format json. 


Les étapes
-----------

Au moment de la connexion, les données du jeu vous sont communiquées. Les détails sur la structure de données employée est dans la section :ref:`initialisation`.

Ensuite, la première chose que votre client doit envoyer au serveur est un dictionnaire contenant une entrée ``"nickname"`` et contenant le nom que votre *joueur* aura pendant la partie.

Lorsque le jeu est en cours, vous pouvez passer des ordres. Le format des ordres est décrit dans la section :ref:`ordres`. Le dernier ordre passé avant le tour de jeu est celui qui sera utilisé. 

Chaque seconde, un tour se termine et un autre commence. Le serveur vous envoie les informations sur ce qu'il s'est passé en utilisant le format décrit dans la section : :ref:`retours` .


Résolution des ordres par le serveur
+++++++++++++++++++++++++++++++++++++

A chaque tour, voici ce que fait le serveur:

.. code::

  Pour chaque joueur (pris dans un ordre non défini):
     Pour chaque action de l'ordre du joueur:
        Traiter l'action

Remarque : l'ordre du joueur est la dernière liste reçue depuis le dernier tour.


.. _initialisation:

Initialisation
+++++++++++++++

Le monde est la donnée de différents objets. Sur le dessin suivant on voit tous les objets possibles : 

.. image:: schema_jeu.png
  :width: 80%


On distingue trois catégories d'objets : 

* Les tanks

* Les projectiles

* Les objets fixes : mur (cassable ou non) et bonus

Au moment de la connexion, le serveur vous envoie l'état du monde sous la forme d'un dictionnaire. Au moment du démarrage du jeu, le même envoi est à nouveau effectué (éventuellement il contient plus d'informations car d'autres joueurs se sont connectés entre temps).

* à la clé ``"joueurs"``, il y a la liste des *tanks*. Chaque *tank* est un dictionnaire : 

  .. code::

     {"id": idDuTank,
      "position": positionDuTank, 
      "direction": directionDuTank, 
      "score": scoreActuel} 
    
  où:
  
  * ``idDuTank`` est un entier,
  
  * ``positionDuTank`` est une liste de 2 entiers : la position en x et celle en y,
   
  * ``direction`` est une liste de deux entiers : (directionX, directionY) et
   
  *  ``scoreActuel`` est un entier représentant le score actuel du joueur. 

* à la clé ``"idJoueur"``, il y a un entier qui est l'identifiant qui vous a été attribué,

* à la clé ``"map"``, il y a la liste des objets actuels de la carte :
  
  Un objet de carte est représenté par un dictionnaire de ses propriétés. Par
  exemple, un mur indestructible en position (0,5) est donné par ``{'cassable': false, 'pos':[0,5]}``. Un bonus valant 10 points en position (10,10) par ``{'points' : 10, 'pos':[10,10]}``






.. _ordres:

Les ordres du client
+++++++++++++++++++++++

Chaque tour vous pouvez envoyer un ordre (une liste d'actions). Le dernier ordre reçu sera celui
retenu par le serveur au moment d'exécuter les ordres du tour. 
A la fin de votre chaine JSON, ajoutez un ``\n`` pour informer le serveur de la fin de l'ordre. 

Un ordre est une liste de chaines (chaque chaine représente une action). Les chaines possibles sont : 

* ``"hrotate"`` : rotation horaire,

* ``"trotate"`` : rotation anti-horaire ou "trigo". 

* ``"move"`` : déplacement d'une case en avant

* ``"shoot"`` : une balle est émise


Chaque tour, un *tank* peut faire au plus UNE action de mouvement (avancer ou tourner) et au plus UNE action de tir.

Par exemple : 

* ``["hrotate", "shoot"]`` signifie qu'on veut tourner et tirer (d'abord on tourne, ensuite on tire)

* ``["shoot", "hrotate"]`` signifie qu'on veut tirer et tourner (d'abord on tire, ensuite on tourne)

* ``["move", "shoot"]`` signifie qu'on veut avancer d'une case et tirer

* ``["move"]`` signifie qu'on veut avancer d'une case (mais on ne tire pas)

* ``[]`` signifie qu'on ne veut rien faire à ce tour


* ``["shoot", "hrotate", "shoot"]`` n'est pas correct : on ne peut faire que deux actions au maximum

* ``["hrotate", "move"]`` n'est pas correct : on ne peut faire qu'une seule action de mouvement au maximum

* ``["shoot", "shoot"]`` n'est pas correct : on ne peut faire qu'une seule action tir au maximum



Remarques :

* Le tank ne peut donc pas faire plus de 2 actions. 

* Le tank peut ne pas tirer.

* La liste des ordres peut être vide ou réduite à un seul élément.

* Pour récupérer un bonus, pas d'action spéciale, il suffit de passer dessus.


.. _retours:

Les retours du serveur
+++++++++++++++++++++++++++++++++++

A chaque tour, le serveur fait évoluer le monde et renvoie une liste d'informations sur les *événements* qui se sont passés. 

Un *événement* qui s'est passé peut être soit :

* déplacement/rotation d'un joueur,

* déplacement d'un projectile,

* tir d'un projectile,

* mort d'un joueur,

* connexion d'un nouveau joueur,

La disparition d'un bonus ou d'un mur n'est pas une information transmise par le serveur. A vous de vous en apercevoir. 


Structures de données
----------------------

Pour chaque *évènement* la structure de données est particulière. Vous trouverez la liste ci-dessous. Remarquez qu'il y a des données inutiles ou redondantes ! A vous de faire le tri !

* déplacement d'un *tank* : ``['joueur', 'move', idJoueur]``  où ``idJoueur`` est un entier,

* rotation d'un *tank* : ``['joueur', 'rotate', idjoueur, [directionX, directionY]]``  où ``idJoueur`` est un entier, ``directionX`` et ``directionY``. Le vecteur (directionX,directionY) représente la nouvelle direction du joueur,


* récupération d'un bonus par un joueur : ``['joueur', 'recupere_bonus', idJoueur, [posX, posY]]`` avec ``posX`` et ``posY`` deux entiers,

* déplacement d'un projectile : ``['projectile', 'move', idProjectile, [newPosX, newPosY]]`` avec idProjectile un entier, newPosX et newPosY deux entiers,

* tir d'un projectile : ``['joueur', 'shoot', idJoueur, idProjectile, [positionX, positionY], [directionX, directionY]]`` avec ``idJoueur``, ``idProjectile``, ``positionX``, ``positionY``, ``directionX`` et ``directionY`` des entiers. Le couple (directionX,directionY) est le vecteur représentant la direction du tir.


* explosion d'un projectile : ``['projectile', 'explode', idProjectile, [posX, posY], listeDePosition, Booléen]`` où ``posX`` et ``posY`` sont la position de l'explosition, ``listeDePosition`` est la liste des positions des murs qui ont explosé, et le ``Booléen`` est à Vrai si cette explosion a tué un *tank*. 


* re-apparition d'un tank d'un endroit à un autre :  ``['joueur', 'respawn', idJoueur, [posX, posY]`` où ``posX`` et ``posY`` sont la position de l'endroit où re-apparaît le tank.



