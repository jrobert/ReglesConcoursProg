Principe Client/Serveur
==========================

Votre rendu se présentera sous la forme d'un client qui se connecte à un
serveur.
Les clients envoient des instructions (déplacer / tirer/ tourner / etc. ) et le serveur envoie régulièrement aux clients l'état du monde (position des tanks, structure représentant le monde, etc. )

La communication avec le serveur suit un protocole détaillé dans la section :ref:`protocole`. 


Serveur
++++++++

Le code du serveur est disponible sur le dépôt git suivant : `https://gitlab.com/jrobert/concoursProgIUT.git <https://gitlab.com/jrobert/concoursProgIUT.git>`_ .

Vous pouvez le cloner pour le récupérer sur votre machine en faisant : ``git clone https://gitlab.com/jrobert/concoursProgIUT.git``. 

Pour l'exécuter, suivez les instructions données dans le fichier README. 

Vous pouvez interagir avec le serveur dans le terminal avec quelques commandes : 

   * ``start`` : permet de démarrer le jeu

   * ``pause`` : met le jeu en pause (start pour reprendre)

   * ``list`` : affiche la liste des clients connectés

   * ``verbosity`` pour gérer la verbosité (niveau des messages d'erreur). Par exemple : ``verbosity 0`` pour avoir tous les messages ou ``verbosity 10`` ou ``verbosity 20`` ou ``verbosity 30`` ou ``verbosity 40`` ou encore ``verbosity 50`` pour le minimum de messages.

   * ``Ctrl-C`` pour quitter le serveur.


Visualisation du jeu
+++++++++++++++++++++

Une fois le serveur lancé, vous devriez pouvoir visualiser le jeu à l'adresse : 
`http://localhost:8081/ <http://localhost:8081>`_


**Attention** : il peut y avoir des petits défauts d'affichage si vous ouvrez la page de visualisation après avoir lancé la commande ``start`` sur le serveur. 
Dans ce cas, rechargez la page de visualisation pour avoir l'état correct du monde.


Client
++++++++

Le client doit se connecter avec le protocole TCP au serveur. Par défaut le serveur écoute sur le port ``8889``.
Les échanges qui se font par la suite avec le serveur sont en ``json``. 


.. topic:: Important

  Votre client doit être un exécutable **client** situé dans le dossier ``~/rendu/`` et qu'on lance de la manière suivante : 

  .. code::

     ~/rendu/client -p PORT ADRESSE_SERVEUR

  Par exemple : 
  
  .. code::

     ~/rendu/client -p 8889 info22-01.iut45.univ-orleans.fr




Exemple(s) de client(s)
------------------------

En python
**********
.. literalinclude:: client_python.py
   :language: python

En Java
*********

Vous devez télécharger les fichiers suivants :

* :download:`Client.java <ClientJava/Client.java>`

* :download:`Executable.java <ClientJava/Executable.java>`

* :download:`gson-2.8.2.jar <ClientJava/gson-2.8.2.jar>`

Pour compiler : ``javac -cp gson-2.8.2.jar:. Executable.java``

Pour exécuter : ``java -cp gson-2.8.2.jar:. Executable Hote Port`` 


