import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.IOException;

import java.net.Socket;
import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.Map;
import java.util.HashMap;

public class Client{

    private BufferedReader  input;
    private OutputStreamWriter output;
    private Gson gson;

    private int idJoueur;


    Client(String hote, int port) throws UnknownHostException, IOException{
        Socket s = new Socket(InetAddress.getByName(hote),port); 
        input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        output = new OutputStreamWriter(s.getOutputStream());
        gson  = new GsonBuilder().create();
    }

    public void send(Object a){
        gson.toJson(a, output);
				try{
				output.flush();
				}
				catch(IOException e){
					System.out.println(e);				
				}
    }


    public Object read() throws IOException{
       String s = input.readLine();
       System.out.println("Données reçues:" + s);
       return gson.fromJson(s, Object.class);
    }


    // Le suppressWarning permet de ne pas faire de message de warning à la compilation liés au transtypage de ce que renvoie read.
    @SuppressWarnings("unchecked")
    public void connexion(String nickname) throws IOException{
            // CECI n'est qu'un exemple de code montrant comment recevoir et envoyer des données au serveur,
            
            // La méthode read renvoie un object, mais je sais qu'en fait ici j'attends un dictionnaire (Map) de String vers d'autres choses. 
            // Ici, le map attendu est qqch comme: { "idJoueur": 2 }
            Map<String,Object> ordres =(Map<String,Object>) read();

            // Récupération de l'IdJoueur. La bibliothèque l'a parsé en Double, d'où le intValue. 
            idJoueur = ((Double) ordres.get("idJoueur")).intValue();

            // Envoie de données : 
            // 1) Créer la structure de données
            HashMap<String,String> h = new HashMap<String,String>();
            h.put("nickname",nickname);
            // 2) Appeler la méthode send.
            send(h);
            
            // Autres réceptions de données (peut être à mettre ailleurs, et à continuer !! )
            Map<String,Object> carte =(Map<String,Object>) read();
            for( Map<String,Object> donneesCase : (ArrayList<Map<String,Object>>)carte.get("map"))
            {
                ArrayList<Double> position = (ArrayList<Double>) donneesCase.get("pos");
                boolean cassable =  (boolean)donneesCase.get("cassable");
                System.out.println("Case en position x=" + position.get(0)+ ", y=" + position.get(1) + " est cassable : " + cassable);
            }
            for( Map<String,Object> donneesJoueurs : (ArrayList<Map<String,Object>>)carte.get("joueurs"))
                System.out.println(donneesJoueurs.get("id"));

    }

}
