import java.net.UnknownHostException;
import java.io.IOException;

public class Executable{

    public static void main(String[] args){
        try{
            Client c = new Client(args[0], Integer.parseInt(args[1]));
            c.connexion("John");
        }
        catch(UnknownHostException e){
            System.out.println("Usage : Executable nomHote port."); 
            System.out.println("Hote inconnu");
            System.exit(0);
        }
        catch(IOException e){
            System.out.println("Erreur de connexion"); 
            System.out.println(e); 
            System.exit(0);
        }

            
    }


}
