Présentation du concours
+++++++++++++++++++++++++

Si vous avez connu la NES, peut être avez vous joué à "Battle City" ? 

.. image:: nes.jpeg
  :width: 30%


Si ce n'est pas le cas, il n'est jamais trop tard, vous pouvez jouer `ici <http://www.retrogames.cz/play_014-NES.php>`_  ... mais ce n'est pas le but. 



L'objectif de ce concours est de développer une IA pour un clone de ce jeu. 

Voici à quoi ce clone ressemble : 

.. image:: zone_jeu.png
  :width: 80%

Votre IA devra contrôler un *tank* qui peut avancer, tourner et tirer.  Chaque seconde le
serveur accomplit les ordres demandés par chaque IA. A chaque "tour de jeu" (c'est à dire chaque seconde) un
*joueur* peut effectuer jusqu'à deux actions parmi les actions suivantes :

   * faire un mouvement : avancer ou tourner
   
   * tirer

L'objectif est d'obtenir plus de points de victoire que les autres IA. On obtient ces points de victoire de deux façons :

   * en récupérant des bonus disséminés sur le plateau de jeu (on récupère un bonus en "marchant" dessus)
   
   * en tirant sur les tanks adverses. 


